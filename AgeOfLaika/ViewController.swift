//
//  ViewController.swift
//  AgeOfLaika
//
//  Created by Neil on 28/10/2014.
//  Copyright (c) 2014 Coupaman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var dogYearsLabel: UILabel!
    @IBOutlet weak var humanAgeTextField: UITextField!
    @IBOutlet weak var convertButton: UIButton!
    @IBOutlet weak var enterYearsPrompt: UILabel!
    @IBOutlet weak var convertRealButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func humanYearsChanged(sender: UITextField) {
        convertButton.hidden = !(humanAgeTextField.text.toInt() != nil)
        convertRealButton.hidden = convertButton.hidden
        enterYearsPrompt.hidden = !convertButton.hidden
    }
    
    
    @IBAction func convertButtonPressed(sender: UIButton) {
        let dogAgeMultiplier = 7
        let humanAge = humanAgeTextField.text.toInt()!
        dogYearsLabel.text = "Age in human years is \(humanAge * dogAgeMultiplier)"
        humanAgeTextField.resignFirstResponder()
//      self.view.endEditing(true)
    }

    @IBAction func convertRealButtonPressed(sender: AnyObject) {
        let humanAge = Double(humanAgeTextField.text.toInt()!)
        var realDogAge:Double
        
        if humanAge <= 2 {
            realDogAge = humanAge * 10.5
        }
        else {
            realDogAge = (10.5 * 2) + ((humanAge-2)*4)
        }
        dogYearsLabel.text = "Real age is \(realDogAge) in Human years"
        humanAgeTextField.resignFirstResponder()
    }
}
